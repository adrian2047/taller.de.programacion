//Autor: Adrian Laurente

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResult();
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
 int time;
 float day;
 float hour;
 float minute;
 int a;
 /*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResult();
}
//=====================================================================================================
void CollectData(){
	cout<<"Inserte la cantidad de minutos"<<endl;
	cin>>time;
}
//=====================================================================================================	
void Calculate(){
	day = (time/1440);
	a = (time%1440);
	hour = (a/60);
	minute = (a%60);
}
//=====================================================================================================	
void ShowResult(){
	cout<<"==================================="<<endl;
	cout<<time<<" Es equivalente a:"<<endl;
	if (day<=1){
		cout<<day<<" dia"<<endl;
	} else{
		cout<<day<<" dias"<<endl;
		
	}
	if (hour<=1){
		cout<<hour<<" hora"<<endl;
	} else{
		cout<<hour<<" horas"<<endl;
	}
	if (minute<=1){
		cout<<minute<<" minuto"<<endl;
	} else {
		cout<<minute<<" minutos"<<endl;
	}
}


	
	
