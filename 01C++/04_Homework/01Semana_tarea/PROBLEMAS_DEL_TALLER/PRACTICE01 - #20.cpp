/*Ingresar dos lados de un triangulo y el �ngulo que forman, 
e imprima el  valor del tercer lado,  los otros dos �ngulos y 
el �rea del tri�ngulo.  */


#include <iostream>
#include <math.h>  //BIBLIOTECA MATEMATICA 

using namespace std;
int main(){
	
	//Declaration y initialize
	double a,b,c,x,y,z,area = 0;
	double PI = 3.14159;
	
	//Display phrase 1
	cout<<"Ingrese el primer lado del triangulo : ";
	cin>>a;
	
	cout<<"Ingrese el segundo lado del triangulo : ";
	cin>>b;
	
	cout<<"Ingrese el angulo que forman los dos lados mencionados : ";
	cin>>x;
	
	//Operation
	c = sqrt( pow(a,2) + pow(b,2) - 2*a*b*cos(x*PI/180) );
	area = a*b*sin(x*PI/180)/2;
	y = asin(b*sin(x*PI/180)/c);
	z = asin(a*sin(x*PI/180)/c);
	
	//Display phrase 2
	cout<<"El Tercer lado es:"<<c<<endl;
	cout<<"El Area del triangulo es: "<<area<<endl;
	cout<<"El Segundo angulo del triangulo es: "<<y<<endl;
	cout<<"El Tercer angulo del triangulo es: "<<z<<endl;

return 0;
}
