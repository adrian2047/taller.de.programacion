/*17.-  El �rea de una elipse se obtiene con la f�rmula piab , donde a es el radio menor de la elipse y b es el radio mayor,
 y su per�metro se obtiene con la f�rmula pi[4(a+b)2]0.5. 
Realice un programa en C ++ utilizando estas f�rmulas y calcule el �rea y el per�metro de una elipse que tiene 
un radio menor de 2.5 cm y un radio mayor de 6.4 cm
*/

#include <iostream>

using namespace std;

//Declaration e initialization of  the variables
//Global varaibles

float pi=3.14;
float aInput=0.0;
float bInput=0.0;
float perimeter=0.0;
float area=0.0;

//Function declaration

void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationPerimeter(float a, float b);
float calculationArea(float a, float b);

//Main

int main(){
	
	Run();
    return 0;
    
}

void Run(){
	
	CollectData();
	Calculate();
	ShowResults();

}

void CollectData(){
	
	cout<<"=========InsertData==========\r\n";
	cout<<"Calculation of the perimeter\r\n";
	cout<<"Enter the minor radious of the elipse: ";
	cin>>aInput;
	cout<<"Enter the mayor radius of the elipse: ";
	cin>>bInput;
	
}

void Calculate(){
	
    perimeter=calculationPerimeter(aInput, bInput);
    area=calculationArea(aInput, bInput);
    
}

void ShowResults(){
	
	cout<<"==========Show result==========\r\n";
	cout<<"The perimeter of the elipse is: "<<perimeter<<"\r\n";
	cout<<"The area of the elipse is: "<<area<<"\r\n";
	
}

float calculationPerimeter(float a, float b){
	
	return pi*(4*(a+b))*0.5;
	
}

float calculationArea(float a, float b){
	
	return pi*(a*b);
	
}










































