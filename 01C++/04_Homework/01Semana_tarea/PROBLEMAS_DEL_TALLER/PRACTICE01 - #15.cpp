//Exercise 15
//Adrian Laurente
//xanadu0791@gmail.com

#include <iostream>

using namespace std;

//Declaration e initialization of the variables

float X1Input=0; //Point 1 of the abcissa
	
float Y1Input=0; //Point 1 of the ordinate
	
float X2Input=0.0; //Point 2 of the abcissa
	
float Y2Input=0.0; //Point 3 of the ordinate
	
float Slopeoftheline=0.0; //Slope of the line
	
float midPointAbcissa=0.0; //Midpoint of the line in the abcissa
	
float midPointOrdenate=0.0; //Midpoint of the line in the ordenate

//Function declaration

void Run();
void CollectData();
void Calculate();
void ShowResults();
float calculationSlope(float X1, float X2, float Y1, float Y2);
float calculationmidpointAbcissa(float X1, float X2);
float calculationmidpointOrdenate(float Y1, float Y2);


int main(){
	
	Run();
	return 0;
}

void Run(){
	
	CollectData();
	Calculate();
	ShowResults();
	
}

void CollectData(){
	
	cout<<"=========Insert data=========\r\n";
	cout<<"Calculation of the slope\r\n";
	cout<<"\tEnter the value of the first abcissa: ";
	cin>>X1Input;
	cout<<"\tEnter the value of the second abcissa: ";
	cin>>X2Input;
	cout<<"\tEnter the value of the first ordenate: ";
	cin>>Y1Input;
	cout<<"\tEnter the value of the second ordenate: ";
	cin>>Y2Input;
	
}
	
void Calculate(){
	
	Slopeoftheline=calculationSlope(X1Input, X2Input, Y1Input, Y2Input);
	midPointAbcissa=calculationmidpointAbcissa(X1Input, X2Input);
	midPointOrdenate=calculationmidpointOrdenate(Y1Input, Y2Input);
	
}	
	
void ShowResults(){
	
	cout<<"==========Show results==========\r\n";
	cout<<"The slope of the line is: "<<Slopeoftheline<<"\r\n";
	cout<<"The midpoint of the line is:"<<"("<<midPointAbcissa<<","<<midPointOrdenate<<")"<<"\r\n";
	
}	

//Operation of the variables

float calculationSlope(float X1, float X2, float Y1, float Y2){
	
	return (Y2-Y1)/(X2-X1);
	
} 

float calculationmidpointAbcissa(float X1, float X2){
	
	return (X1+X2)/2;
	
}	
	
float calculationmidpointOrdenate(float Y1, float Y2){
	
	return (Y1+Y2)/2;
	
}	
	
